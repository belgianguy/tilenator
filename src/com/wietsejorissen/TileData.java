package com.wietsejorissen;

/**
 * Created by wietse on 17/06/17.
 */
public class TileData {
    private int[][] quarters = new int[2][2];

    private QuarterTile[] quarterTiles = new QuarterTile[4];

    //NW SW NE SE

    public void setNorthWest(int pieceNumber, double angle) {
        quarterTiles[0] = new QuarterTile(pieceNumber, angle);
    }

    public void setSouthWest(int pieceNumber, double angle) {
        quarterTiles[1] = new QuarterTile(pieceNumber, angle);
    }

    public void setNorthEast(int pieceNumber, double angle) {
        quarterTiles[2] = new QuarterTile(pieceNumber, angle);
    }

    public void setSouthEast(int pieceNumber, double angle) {
        quarterTiles[3] = new QuarterTile(pieceNumber, angle);
    }

    public QuarterTile[] getQuarterTiles() {
        return quarterTiles;
    }

    public void setNorthWest(QuarterTile quarterTile) {
        quarterTiles[0] = quarterTile;
    }

    public void setSouthWest(QuarterTile quarterTile) {
        quarterTiles[1] = quarterTile;
    }

    public void setNorthEast(QuarterTile quarterTile) {
        quarterTiles[2] = quarterTile;
    }

    public void setSouthEast(QuarterTile quarterTile) {
        quarterTiles[3] = quarterTile;
    }


}
