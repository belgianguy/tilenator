package com.wietsejorissen;

/**
 * Created by wietse on 25/05/17.
 */
public enum Orientation {

    NORTH_EAST(1),
    SOUTH_EAST(2),
    SOUTH_WEST(4),
    NORTH_WEST(8),
    NORTH(16),
    EAST(32),
    SOUTH(64),
    WEST(128);

    private int value;

    private Orientation(int value) {
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    public static Orientation get(int code) {
        switch(code) {
            case  1: return NORTH_EAST;
            case  2: return SOUTH_EAST;
            case  4: return SOUTH_WEST;
            case  8: return NORTH_WEST;
            case  16: return NORTH;
            case  32: return EAST;
            case  64: return SOUTH;
            case  128: return WEST;
        }
        throw new IllegalArgumentException("Unknown orientation: " + code);
    }


}


