package com.wietsejorissen;

/**
 * Created by wietse on 17/06/17.
 */
public class TileUtils {

    public static void printTile(TileData tileData) {
        System.out.println(tileData.getQuarterTiles()[0].getPieceNumber() + " " + tileData.getQuarterTiles()[2].getPieceNumber());
        System.out.println(tileData.getQuarterTiles()[1].getPieceNumber() + " " + tileData.getQuarterTiles()[3].getPieceNumber());

        System.out.println("Piece NW " + tileData.getQuarterTiles()[0].getPieceNumber() + " :: " + tileData.getQuarterTiles()[0].getAngle() + " degrees");
        System.out.println("Piece NE " + tileData.getQuarterTiles()[2].getPieceNumber() + " :: " + tileData.getQuarterTiles()[2].getAngle() + " degrees");
        System.out.println("Piece SW " + tileData.getQuarterTiles()[1].getPieceNumber() + " :: " + tileData.getQuarterTiles()[1].getAngle() + " degrees");
        System.out.println("Piece SE " + tileData.getQuarterTiles()[3].getPieceNumber() + " :: " + tileData.getQuarterTiles()[3].getAngle() + " degrees");
    }

    public static TileData rotateTileBy90DegreesCW(TileData tileData) {
        QuarterTile NW = tileData.getQuarterTiles()[0];
        QuarterTile SW = tileData.getQuarterTiles()[1];
        QuarterTile NE = tileData.getQuarterTiles()[2];
        QuarterTile SE = tileData.getQuarterTiles()[3];

        tileData.setNorthEast(NW);
        tileData.setSouthEast(NE);
        tileData.setSouthWest(SE);
        tileData.setNorthWest(SW);

        return tileData;
    }

    public static TileData rotateTileBy90DegreesCCW(TileData tileData) {
        QuarterTile NW = tileData.getQuarterTiles()[0];
        QuarterTile SW = tileData.getQuarterTiles()[1];
        QuarterTile NE = tileData.getQuarterTiles()[2];
        QuarterTile SE = tileData.getQuarterTiles()[3];

        tileData.setNorthEast(SE);
        tileData.setSouthEast(SW);
        tileData.setSouthWest(NW);
        tileData.setNorthWest(NE);

        return tileData;
    }

}
