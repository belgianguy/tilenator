package com.wietsejorissen;

/**
 * Created by wietse on 17/06/17.
 */
public class QuarterTile {
    private int pieceNumber;
    private double angle;

    public QuarterTile(int pieceNumber, double angle) {
        this.pieceNumber = pieceNumber;
        this.angle = angle;
    }

    public int getPieceNumber() {
        return pieceNumber;
    }

    public double getAngle() {
        return angle;
    }
}
