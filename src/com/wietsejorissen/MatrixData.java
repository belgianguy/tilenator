package com.wietsejorissen;

/**
 * Created by wietse on 1/06/17.
 */
public class MatrixData {

    private int[][] matrix;
    private int[][] waterArea;
    private int angle;
    private int neighbourSum;

    public MatrixData(int[][] matrix, int angle) {
        this.matrix = matrix;
        this.neighbourSum = calculateNeighbourSum(matrix);
        this.angle = angle;
    }

    public MatrixData() {
        this.matrix = null;
        this.angle = -1;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
        this.neighbourSum = calculateNeighbourSum(matrix);
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int[][] getWaterArea() {
        return waterArea;
    }

    public void setWaterArea(int[][] waterArea) {
        this.waterArea = waterArea;
    }

    public int getNeighbourSum() {
        return neighbourSum;
    }

    private int calculateNeighbourSum(int[][] matrix) {
        int sum = 0;

        if(matrix[0][0] != 0) sum+= Orientation.NORTH_WEST.getValue();
        if(matrix[0][1] != 0) sum+= Orientation.NORTH.getValue();
        if(matrix[0][2] != 0) sum+= Orientation.NORTH_EAST.getValue();

        if(matrix[1][0] != 0) sum+= Orientation.WEST.getValue();
        if(matrix[1][1] != 0) sum+= 0;
        if(matrix[1][2] != 0) sum+= Orientation.EAST.getValue();

        if(matrix[2][0] != 0) sum+= Orientation.SOUTH_WEST.getValue();
        if(matrix[2][1] != 0) sum+= Orientation.SOUTH.getValue();
        if(matrix[2][2] != 0) sum+= Orientation.SOUTH_EAST.getValue();

        return sum;
    }
}
