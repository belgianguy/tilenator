package com.wietsejorissen;

import java.util.Optional;

/**
 * Created by wietse on 5/06/17.
 */
public class MatrixUtils {
    public static void printMatrix(int[][] matrix) {
        for(int i =0; i < matrix.length;++i) {
            for(int j =0; j < matrix[0].length;++j) {
                if(i == 1 && j == 1) {
                    System.out.print("X ");
                } else {
                    System.out.print(matrix[i][j] + " ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void printAngle(int angle) {
        System.out.println("Angle: " + angle + " degrees");
    }

    public static int calculateNeigbours(int i, int j, boolean[][] array) {
        int sum = 0;

        sum += calculateNorthWest(i,j,array);
        sum += calculateNorth(i,j,array);
        sum += calculateNorthEast(i,j,array);
        sum += calculateEast(i,j,array);
        sum += calculateSouthEast(i,j,array);
        sum += calculateSouth(i,j,array);
        sum += calculateSouthWest(i,j,array);
        sum += calculateWest(i,j,array);

        return sum;
    }

    public static Optional<boolean[][]> calculateWaterArea(boolean[][] array) {
        int rows = array.length;
        int cols = array[0].length;
        boolean hasWater = false;
        boolean waterArea[][] = new boolean[rows][cols];

        for(int i = 0; i < rows; ++i) {
            for(int j = 0; j < cols; ++j) {
                boolean isWater = array[i][j] && calculateNeigbours(i, j, array) == 255;
                waterArea[i][j] = isWater;
                if(!hasWater && isWater) {
                    hasWater = true;
                }
            }
        }
        if(hasWater) {
            return Optional.of(waterArea);
        }
        return Optional.empty();
    }

    private static int calculateNorthWest(int i, int j, boolean[][] array) {
        if(i == 0 || j == 0 || !array[i - 1][j - 1]) {
            return 0;
        } else {
            return Orientation.NORTH_WEST.getValue();
        }
    }

    private static int calculateNorthEast(int i, int j, boolean[][] array) {
        if(i == 0 || j == array[0].length - 1 || !array[i - 1][j + 1]) {
            return 0;
        } else {
            return Orientation.NORTH_EAST.getValue();
        }
    }

    private static int calculateSouthEast(int i, int j, boolean[][] array) {
        if(i == array.length - 1 || j == array[0].length - 1 || !array[i + 1][j + 1]) {
            return 0;
        } else {
            return Orientation.SOUTH_EAST.getValue();
        }
    }

    private static int calculateSouthWest(int i, int j, boolean[][] array) {
        if(i == array.length - 1 || j == 0 || !array[i + 1][j - 1]) {
            return 0;
        } else {
            return Orientation.SOUTH_WEST.getValue();
        }
    }

    private static int calculateNorth(int i, int j, boolean[][] array) {
        if(i == 0 || !array[i - 1][j]) {
            return 0;
        } else {
            return Orientation.NORTH.getValue();
        }
    }

    private static int calculateEast(int i, int j, boolean[][] array) {
        if(j == array[0].length - 1 || !array[i][j + 1]) {
            return 0;
        } else {
            return Orientation.EAST.getValue();
        }
    }

    private static int calculateSouth(int i, int j, boolean[][] array) {
        if(i == array.length - 1 || !array[i + 1][j]) {
            return 0;
        } else {
            return Orientation.SOUTH.getValue();
        }
    }

    private static int calculateWest(int i, int j, boolean[][] array) {
        if(j == 0 || !array[i][j - 1]) {
            return 0;
        } else {
            return Orientation.WEST.getValue();
        }
    }

    public static void printNeighbourSum(int neighbourSum) {
        System.out.println("Sum: " + neighbourSum);
    }

    public static void printMatrixCode(int index, int[][] matrix) {
        System.out.print("matrixHolder[" + index + "] = new int[][]{");
        //matrixHolder[0] = new int[][]{{0,0,0},{0,0,0},{0,0,0}};
        for(int i =0; i < matrix.length;++i) {
            System.out.print("{");
            for(int j =0; j < matrix[0].length;++j) {
                if( j == matrix[0].length - 1) {
                    System.out.print(matrix[i][j]);
                } else {
                    System.out.print(matrix[i][j] + ", ");
                }
            }
            if( i == matrix.length - 1) {
                System.out.print("}");
            } else {
                System.out.print("}, ");
            }
        }
        System.out.println("};");

    }
}
