package com.wietsejorissen;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class Main {

    private static final int[][][] uniqueMatrixList = new int [70][3][3];

    static {
        uniqueMatrixList[0] = new int[][]{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
        uniqueMatrixList[1] = new int[][]{{0, 0, 0}, {0, 0, 0}, {0, 0, 1}};
        uniqueMatrixList[2] = new int[][]{{0, 0, 0}, {0, 0, 0}, {0, 1, 0}};
        uniqueMatrixList[3] = new int[][]{{0, 0, 0}, {0, 0, 0}, {0, 1, 1}};
        uniqueMatrixList[4] = new int[][]{{0, 0, 0}, {0, 0, 0}, {1, 0, 1}};
        uniqueMatrixList[5] = new int[][]{{0, 0, 0}, {0, 0, 0}, {1, 1, 0}};
        uniqueMatrixList[6] = new int[][]{{0, 0, 0}, {0, 0, 0}, {1, 1, 1}};
        uniqueMatrixList[7] = new int[][]{{0, 0, 0}, {0, 0, 1}, {0, 1, 0}};
        uniqueMatrixList[8] = new int[][]{{0, 0, 0}, {0, 0, 1}, {0, 1, 1}};
        uniqueMatrixList[9] = new int[][]{{0, 0, 0}, {0, 0, 1}, {1, 0, 0}};
        uniqueMatrixList[10] = new int[][]{{0, 0, 0}, {0, 0, 1}, {1, 0, 1}};
        uniqueMatrixList[11] = new int[][]{{0, 0, 0}, {0, 0, 1}, {1, 1, 0}};
        uniqueMatrixList[12] = new int[][]{{0, 0, 0}, {0, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[13] = new int[][]{{0, 0, 0}, {1, 0, 0}, {0, 0, 1}};
        uniqueMatrixList[14] = new int[][]{{0, 0, 0}, {1, 0, 0}, {0, 1, 1}};
        uniqueMatrixList[15] = new int[][]{{0, 0, 0}, {1, 0, 0}, {1, 0, 1}};
        uniqueMatrixList[16] = new int[][]{{0, 0, 0}, {1, 0, 0}, {1, 1, 1}};
        uniqueMatrixList[17] = new int[][]{{0, 0, 0}, {1, 0, 1}, {0, 0, 0}};
        uniqueMatrixList[18] = new int[][]{{0, 0, 0}, {1, 0, 1}, {0, 0, 1}};
        uniqueMatrixList[19] = new int[][]{{0, 0, 0}, {1, 0, 1}, {0, 1, 0}};
        uniqueMatrixList[20] = new int[][]{{0, 0, 0}, {1, 0, 1}, {0, 1, 1}};
        uniqueMatrixList[21] = new int[][]{{0, 0, 0}, {1, 0, 1}, {1, 0, 0}};
        uniqueMatrixList[22] = new int[][]{{0, 0, 0}, {1, 0, 1}, {1, 0, 1}};
        uniqueMatrixList[23] = new int[][]{{0, 0, 0}, {1, 0, 1}, {1, 1, 0}};
        uniqueMatrixList[24] = new int[][]{{0, 0, 0}, {1, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[25] = new int[][]{{0, 0, 1}, {0, 0, 0}, {1, 0, 0}};
        uniqueMatrixList[26] = new int[][]{{0, 0, 1}, {0, 0, 0}, {1, 0, 1}};
        uniqueMatrixList[27] = new int[][]{{0, 0, 1}, {0, 0, 0}, {1, 1, 0}};
        uniqueMatrixList[28] = new int[][]{{0, 0, 1}, {0, 0, 0}, {1, 1, 1}};
        uniqueMatrixList[29] = new int[][]{{0, 0, 1}, {0, 0, 1}, {1, 0, 0}};
        uniqueMatrixList[30] = new int[][]{{0, 0, 1}, {0, 0, 1}, {1, 0, 1}};
        uniqueMatrixList[31] = new int[][]{{0, 0, 1}, {0, 0, 1}, {1, 1, 0}};
        uniqueMatrixList[32] = new int[][]{{0, 0, 1}, {0, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[33] = new int[][]{{0, 0, 1}, {1, 0, 0}, {0, 0, 1}};
        uniqueMatrixList[34] = new int[][]{{0, 0, 1}, {1, 0, 0}, {0, 1, 0}};
        uniqueMatrixList[35] = new int[][]{{0, 0, 1}, {1, 0, 0}, {0, 1, 1}};
        uniqueMatrixList[36] = new int[][]{{0, 0, 1}, {1, 0, 0}, {1, 0, 1}};
        uniqueMatrixList[37] = new int[][]{{0, 0, 1}, {1, 0, 0}, {1, 1, 0}};
        uniqueMatrixList[38] = new int[][]{{0, 0, 1}, {1, 0, 0}, {1, 1, 1}};
        uniqueMatrixList[39] = new int[][]{{0, 0, 1}, {1, 0, 1}, {0, 0, 1}};
        uniqueMatrixList[40] = new int[][]{{0, 0, 1}, {1, 0, 1}, {0, 1, 0}};
        uniqueMatrixList[41] = new int[][]{{0, 0, 1}, {1, 0, 1}, {0, 1, 1}};
        uniqueMatrixList[42] = new int[][]{{0, 0, 1}, {1, 0, 1}, {1, 0, 0}};
        uniqueMatrixList[43] = new int[][]{{0, 0, 1}, {1, 0, 1}, {1, 0, 1}};
        uniqueMatrixList[44] = new int[][]{{0, 0, 1}, {1, 0, 1}, {1, 1, 0}};
        uniqueMatrixList[45] = new int[][]{{0, 0, 1}, {1, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[46] = new int[][]{{0, 1, 0}, {0, 0, 1}, {1, 0, 1}};
        uniqueMatrixList[47] = new int[][]{{0, 1, 0}, {0, 0, 1}, {1, 1, 0}};
        uniqueMatrixList[48] = new int[][]{{0, 1, 0}, {0, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[49] = new int[][]{{0, 1, 0}, {1, 0, 1}, {0, 1, 0}};
        uniqueMatrixList[50] = new int[][]{{0, 1, 0}, {1, 0, 1}, {0, 1, 1}};
        uniqueMatrixList[51] = new int[][]{{0, 1, 0}, {1, 0, 1}, {1, 0, 1}};
        uniqueMatrixList[52] = new int[][]{{0, 1, 0}, {1, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[53] = new int[][]{{0, 1, 1}, {0, 0, 0}, {1, 0, 1}};
        uniqueMatrixList[54] = new int[][]{{0, 1, 1}, {0, 0, 0}, {1, 1, 0}};
        uniqueMatrixList[55] = new int[][]{{0, 1, 1}, {0, 0, 0}, {1, 1, 1}};
        uniqueMatrixList[56] = new int[][]{{0, 1, 1}, {0, 0, 1}, {1, 0, 1}};
        uniqueMatrixList[57] = new int[][]{{0, 1, 1}, {0, 0, 1}, {1, 1, 0}};
        uniqueMatrixList[58] = new int[][]{{0, 1, 1}, {0, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[59] = new int[][]{{0, 1, 1}, {1, 0, 0}, {1, 0, 1}};
        uniqueMatrixList[60] = new int[][]{{0, 1, 1}, {1, 0, 0}, {1, 1, 1}};
        uniqueMatrixList[61] = new int[][]{{0, 1, 1}, {1, 0, 1}, {1, 0, 1}};
        uniqueMatrixList[62] = new int[][]{{0, 1, 1}, {1, 0, 1}, {1, 1, 0}};
        uniqueMatrixList[63] = new int[][]{{0, 1, 1}, {1, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[64] = new int[][]{{1, 0, 1}, {0, 0, 0}, {1, 0, 1}};
        uniqueMatrixList[65] = new int[][]{{1, 0, 1}, {0, 0, 0}, {1, 1, 1}};
        uniqueMatrixList[66] = new int[][]{{1, 0, 1}, {0, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[67] = new int[][]{{1, 0, 1}, {1, 0, 1}, {1, 0, 1}};
        uniqueMatrixList[68] = new int[][]{{1, 0, 1}, {1, 0, 1}, {1, 1, 1}};
        uniqueMatrixList[69] = new int[][]{{1, 1, 1}, {1, 0, 1}, {1, 1, 1}};
    }

    private static int MATRIX_LENGTH = 3;
    private static boolean[][] INPUT_MATRIX =   {
                                                    {false,false,true,true,true,false,false},
                                                    {false,true,true,true,true,true,false},
                                                    {true,true,true,true,true,true,true},
                                                    {true,true,true,true,true,true,true},
                                                    {true,true,true,true,true,true,true},
                                                    {false,true,true,true,true,true,false},
                                                    {false,false,true,true,true,false,false},
                                                };

    private static volatile boolean matrixFound;



    public static void main(String[] args) {
        calculateTiles(INPUT_MATRIX);
    }

    private static void calculateTiles(boolean[][] matrix) {
        Optional<boolean[][]> waterAreaOptional = MatrixUtils.calculateWaterArea(matrix);

        for(int i = 0; i < matrix.length;++i) {
            for(int j = 0; j < matrix[0].length;++j) {
                if(matrix[i][j]) {
                    int[][] result = getMatrixForPoint(i, j, matrix);
                    MatrixData matrixData;
                    if(waterAreaOptional.isPresent()) {
                        int[][] waterAreaPoint = getMatrixForPoint(i, j, waterAreaOptional.get());
                        matrixData = getMatrixAndAngleWithWater(result,waterAreaPoint);
                    } else {
                        matrixData = getMatrixAndAngle(result);
                    }

                    TileData tileData = NeighbourSumParser.generate(matrixData);
                }
            }
        }
    }


    private static int[][] getMatrixForPoint(int i, int j, boolean[][] inputMatrix) {
        int[][] result = new int[MATRIX_LENGTH][MATRIX_LENGTH];

        result[0][0] = calculateNorthWest(i,j,inputMatrix);
        result[0][1] = calculateNorth(i,j,inputMatrix);
        result[0][2] = calculateNorthEast(i,j,inputMatrix);

        result[1][0] = calculateWest(i,j,inputMatrix);
        result[1][1] = 0;
        result[1][2] = calculateEast(i,j,inputMatrix);

        result[2][0] = calculateSouthWest(i,j,inputMatrix);
        result[2][1] = calculateSouth(i,j,inputMatrix);
        result[2][2] = calculateSouthEast(i,j,inputMatrix);

        return result;
    }

    private static int calculateNorthWest(int i, int j, boolean[][] array) {
        return !(i == 0 || j == 0 || !array[i - 1][j - 1]) ? 1 : 0;
    }


    private static int calculateNorth(int i, int j, boolean[][] array) {
        return !(i == 0 || !array[i - 1][j]) ? 1 : 0;
    }

    private static int calculateNorthEast(int i, int j, boolean[][] array) {
        return !(i == 0 || j == array[0].length - 1 || !array[i - 1][j + 1]) ? 1 : 0;
    }

    private static int calculateEast(int i, int j, boolean[][] array) {
        return !(j == array[0].length - 1 || !array[i][j + 1]) ? 1 : 0;
    }

    private static int calculateSouth(int i, int j, boolean[][] array) {
        return !(i == array.length - 1 || !array[i + 1][j]) ? 1 : 0;
    }

    private static int calculateWest(int i, int j, boolean[][] array) {
        return !(j == 0 || !array[i][j - 1]) ? 1 : 0;
    }

    private static int calculateSouthEast(int i, int j, boolean[][] array) {
        return !(i == array.length - 1 || j == array[0].length - 1 || !array[i + 1][j + 1]) ? 1 : 0;
    }

    private static int calculateSouthWest(int i, int j, boolean[][] array) {
        return !(i == array.length - 1 || j == 0 || !array[i + 1][j - 1]) ? 1 : 0;
    }

    private static MatrixData getMatrixAndAngleWithWater(int[][] input, int[][] waterArea) {
        final int[][] rotatedWaterArea;
        final MatrixData matrixData = getMatrixAndAngle(input);

        switch(matrixData.getAngle()) {
            case 0: rotatedWaterArea = waterArea;break;
            case 90: rotatedWaterArea = rotateMatrix90DegreesCW(waterArea); break;
            case -90: rotatedWaterArea = rotateMatrix90DegreesCCW(waterArea); break;
            case 180: rotatedWaterArea = rotateMatrix180DegreesCW(waterArea); break;
            default: throw new IllegalArgumentException("Bad value for angle: " + matrixData.getAngle());
        }

        matrixData.setWaterArea(rotatedWaterArea);
        return matrixData;

    }

    private static MatrixData getMatrixAndAngle(int[][] input) {
        final ExecutorService executorService = Executors.newFixedThreadPool(4);
        matrixFound = false;
        final List<Callable<MatrixData>> callables = Arrays.asList(
                callable(new MatrixData(input, 0)),
                callable(new MatrixData(rotateMatrix90DegreesCW(input), 90)),
                callable(new MatrixData(rotateMatrix90DegreesCCW(input), -90)),
                callable(new MatrixData(rotateMatrix180DegreesCW(input), 180)));
        MatrixData result = new MatrixData();
        boolean found = false;
        try {
            final List<Future<MatrixData>> futures = executorService.invokeAll(callables);
            for(int k = 0;!found && k < futures.size(); ++k) {
                if(futures.get(k).get() != null && futures.get(k).get().getMatrix() != null && futures.get(k).get().getAngle() != -1) {
                    result.setAngle(futures.get(k).get().getAngle());
                    result.setMatrix(futures.get(k).get().getMatrix());

                    found = true;
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(!found) {
            throw new RuntimeException("Illegal matrix used!");
        }
        executorService.shutdownNow();
        return result;
    }

    private static Callable<MatrixData> callable(MatrixData matrixData) {
        return () -> {
            for(int[][] currentMatrix : uniqueMatrixList) {
                if(matrixFound) {
                    return null;
                }
                if(deepEqualsIgnoringCenter(currentMatrix, matrixData.getMatrix())) {
                    matrixFound = true;
                    return new MatrixData(currentMatrix, matrixData.getAngle());
                }
            }
            return null;
        };
    }

    private static boolean deepEqualsIgnoringCenter(int[][] left, int[][] right) {
        for(int i = 0; i < left.length; ++i) {
            for(int j = 0; j < left.length; ++j) {
                if(!(i == 1 && j == 1) && left[i][j] != right[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }


    private static int[][] rotateMatrix90DegreesCW(int[][] matrix) {
        int[][] rotated = new int[matrix.length][matrix.length];
        int size = rotated.length;
        int layerCount = size / 2;

        IntStream.range(0, layerCount).forEach(
                layer -> {
                    int first = layer;
                    int last = size - first - 1;

                    IntStream.range(first, last).forEach(
                            element -> {
                                int offset = element - first;

                                int top = matrix[first][element];
                                int rightSide = matrix[element][last];
                                int bottom = matrix[last][last-offset];
                                int leftSide = matrix[last-offset][first];

                                rotated[first][element] = leftSide;
                                rotated[element][last] = top;
                                rotated[last][last-offset] = rightSide;
                                rotated[last-offset][first] = bottom;

                            }
                    );
                }
        );
        return rotated;
    }

    private static int[][] rotateMatrix90DegreesCCW(int[][] matrix) {
        int[][] rotated = new int[matrix.length][matrix.length];
        int size = rotated.length;
        int layerCount = size / 2;

        IntStream.range(0, layerCount).forEach(
                layer -> {
                    int first = layer;
                    int last = size - first - 1;

                    IntStream.range(first, last).forEach(
                            element -> {
                                int offset = element - first;

                                int top = matrix[first][element];
                                int rightSide = matrix[element][last];
                                int bottom = matrix[last][last-offset];
                                int leftSide = matrix[last-offset][first];

                                rotated[first][element] = rightSide;
                                rotated[element][last] = bottom;
                                rotated[last][last-offset] = leftSide;
                                rotated[last-offset][first] = top;
                            }
                    );
                }
        );
        return rotated;
    }

    private static int[][] rotateMatrix180DegreesCW(int[][] matrix) {
        return rotateMatrix90DegreesCW(rotateMatrix90DegreesCW(matrix));
    }
}
