package com.wietsejorissen;

/**
 * Created by wietse on 17/06/17.
 */
public class NeighbourSumParser {

    public static TileData generate(MatrixData matrixData) {
        TileData tileData = new TileData();
        double angle = matrixData.getAngle();

        switch (matrixData.getNeighbourSum()) {
            case 0:
            case 2:
            case 5:
            case 6:
            case 7:
            case 15:
                tileData.setNorthWest(1, 90.0d + angle);
                tileData.setNorthEast(1, 0.0d + angle);
                tileData.setSouthWest(1, 180.0d + angle);
                tileData.setSouthEast(1, -90.0d + angle);

                break;
            case 23:
                tileData.setNorthWest(0, 90.0d + angle);
                tileData.setNorthEast(0, 0.0d + angle);
                tileData.setSouthWest(1, 180.0d + angle);
                tileData.setSouthEast(1, -90.0d + angle);

                break;
            case 36:
            case 37:
            case 38:
            case 39:
                tileData.setNorthWest(1, 90.0d + angle);
                tileData.setNorthEast(0, 0.0d + angle);
                tileData.setSouthWest(1, 180.0d + angle);
                tileData.setSouthEast(0, 180.0d + angle);

                break;
            case 54:
            case 55:
                tileData.setNorthWest(0, 90.0d + angle);
                tileData.setNorthEast(2, 90.0d + angle);
                tileData.setSouthWest(1, 180.0d + angle);
                tileData.setSouthEast(0, 180.0d + angle);

                break;
            case 64:
            case 66:
            case 68:
            case 69:
            case 70:
            case 71:
            case 79:
                tileData.setNorthWest(1, 90.0d + angle);
                tileData.setNorthEast(1, 0.0d + angle);
                tileData.setSouthWest(0, 90.0d + angle);
                tileData.setSouthEast(0, -90.0d + angle);

                break;
            case 85:
            case 87:
                tileData.setNorthWest(0, 90.0d + angle);
                tileData.setNorthEast(0, -90.0d + angle);
                tileData.setSouthWest(0, 90.0d + angle);
                tileData.setSouthEast(0, -90.0d + angle);

                break;
            case 98:
            case 96:
                tileData.setNorthWest(1, 90.0d + angle);
                tileData.setSouthWest(0, 90.0d + angle);
                tileData.setNorthEast(0, 0.0d + angle);
                if(matrixData.getWaterArea() != null && matrixData.getWaterArea()[2][2] == 1) {
                    tileData.setSouthEast(12, 90.0d + angle);
                } else {
                    tileData.setSouthEast(2, 90.0d + angle);
                }

                break;
            case 100:
            case 101:
            case 102:
            case 103:
            case 111:
                tileData.setNorthWest(1, 90.0d + angle);
                tileData.setNorthEast(0, 0.0d + angle);
                tileData.setSouthWest(0, 90.0d + angle);
                if(matrixData.getWaterArea() != null && matrixData.getWaterArea()[2][2] == 1) {
                    tileData.setSouthEast(12, 90.0d + angle);
                } else {
                    tileData.setSouthEast(2, 90.0d + angle);
                }


                break;
            case 116:
            case 117:
            case 118:
            case 119:
                tileData.setNorthWest(0, 90.0d + angle);
                tileData.setNorthEast(3, 0.0d + angle);
                tileData.setSouthWest(0, 90.0d + angle);
                tileData.setSouthEast(3, 0.0d + angle);

                break;
            case 130:
            case 131:
            case 134:
            case 135:
                tileData.setNorthWest(0, 0.0d + angle);
                tileData.setNorthEast(1, 0.0d + angle);
                tileData.setSouthWest(0, 180.0d + angle);
                tileData.setSouthEast(1, -90.0d + angle);

                break;
            case 151:
                tileData.setNorthWest(3, 0.0d + angle);
                tileData.setNorthEast(3, 0.0d + angle);
                tileData.setSouthWest(0, 90.0d + angle);
                tileData.setSouthEast(0, -90.0d + angle);

                break;
            case 162:
            case 163:
            case 164:
            case 165:
            case 167:
                tileData.setNorthWest(0, 0.0d + angle);
                tileData.setNorthEast(0, 0.0d + angle);
                tileData.setSouthWest(0, 180.0d + angle);
                tileData.setSouthEast(0, 180.0d + angle);

                break;
            case 175:
                tileData.setNorthWest(0, 90.0d + angle);
                tileData.setNorthEast(0, -90.0d + angle);
                tileData.setSouthWest(0, 90.0d + angle);
                tileData.setSouthEast(0, -90.0d + angle);

                break;
            case 182:
            case 183:
                tileData.setNorthWest(3, 0.0d + angle);
                tileData.setNorthEast(3, 0.0d + angle);
                tileData.setSouthWest(0, 180.0d + angle);
                tileData.setSouthEast(0, 180.0d + angle);

                break;
            case 193:
            case 194:
            case 195:
            case 197:
            case 198:
            case 199:
                tileData.setNorthWest(0, 0.0d + angle);
                tileData.setNorthEast(1, 0.0d + angle);

                if(matrixData.getWaterArea() != null && matrixData.getWaterArea()[2][0] == 1) {
                    tileData.setSouthWest(12, 0.0d + angle);
                } else {
                    tileData.setSouthWest(2, 0.0d + angle);
                }
                tileData.setSouthEast(0, -90.0d + angle);


                break;
            case 215:
                tileData.setNorthWest(3, 0.0d + angle);
                tileData.setNorthEast(0, -90.0d + angle);
                tileData.setSouthWest(3, 0.0d + angle);
                tileData.setSouthEast(0, -90.0d + angle);

                break;
            case 224:
            case 225:
            case 226:
            case 227:
            case 228:
            case 229:
            case 231:
            case 239:
                tileData.setNorthWest(0, 0.0d + angle);
                tileData.setNorthEast(0, 0.0d + angle);
                tileData.setSouthWest(3, 0.0d + angle);
                tileData.setSouthEast(3, 0.0d + angle);

                break;
            case 230:
                tileData.setNorthWest(0, 0.0d + angle);
                tileData.setNorthEast(0, 0.0d + angle);
                if(matrixData.getWaterArea() != null && matrixData.getWaterArea()[2][1] == 1) {
                    tileData.setSouthWest(10, 0.0d + angle);
                    tileData.setSouthEast(10, 0.0d + angle);
                } else {
                    tileData.setSouthWest(2,0.0d + angle);
                    tileData.setSouthEast(2, 0.0d + angle);
                }

                break;
            case 240:
            case 242:
            case 245:
            case 246:
            case 247:
                if(matrixData.getWaterArea() != null && matrixData.getWaterArea()[1][2] == 1 &&
                        matrixData.getWaterArea()[2][1] == 1 &&
                        matrixData.getWaterArea()[2][2] == 1) {
                    tileData.setNorthWest(2, 0.0d + angle);
                    tileData.setNorthEast(10, -90.0d + angle);
                    tileData.setSouthWest(10, 180.0d + angle);
                    tileData.setSouthEast(11, 90.0d + angle);
                } else {
                    tileData.setNorthWest(3, 0.0d + angle);
                    tileData.setNorthEast(3, 0.0d + angle);
                    tileData.setSouthWest(3, 0.0d + angle);
                    tileData.setSouthEast(3, 0.0d + angle);
                }

                break;
            case 255:
                tileData.setNorthWest(13, 0.0d + angle);
                tileData.setNorthEast(13, 0.0d + angle);
                tileData.setSouthWest(13, 0.0d + angle);
                tileData.setSouthEast(13, 0.0d + angle);

                break;
            case 160:
            case 166:
                tileData.setNorthWest(0, 0.0d + angle);
                tileData.setNorthEast(0, 0.0d + angle);
                tileData.setSouthWest(0, 180.0d + angle);
                tileData.setSouthEast(0, 180.0d + angle);

                break;
            default: throw new IllegalArgumentException("Illegal neighbour sum!" + matrixData.getNeighbourSum());
        }


        if(Double.compare(angle, -90.0d) == 0) {
            return TileUtils.rotateTileBy90DegreesCW(tileData);
        } else if(Double.compare(angle, 90.0d) == 0) {
            return TileUtils.rotateTileBy90DegreesCCW(tileData);
        } else if(Double.compare(angle, 180.0d) == 0) {
            return TileUtils.rotateTileBy90DegreesCW(TileUtils.rotateTileBy90DegreesCW(tileData));
        } else {
            return tileData;
        }

    }

}
